from django.conf.urls import patterns, url, include
from rest_framework.routers import DefaultRouter

# Create a router and register our viewsets with it.
from warehouse import views

router = DefaultRouter()
router.register(r'products', views.ProductViewSet)
router.register(r'tags', views.TagViewSet)
router.register(r'locations', views.LocationViewSet)
router.register(r'pallets', views.PalletViewSet)
router.register(r'setsofproducts', views.SetOfProductViewSet)
router.register(r'readers', views.ReaderViewSet)
router.register(r'moves', views.MoveViewSet)

# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browseable API.
urlpatterns = patterns('',
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
)
