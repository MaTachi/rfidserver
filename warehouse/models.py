from django.db import models


class Product(models.Model):
    id = models.CharField(max_length=32, primary_key=True, unique=True,
                          help_text='Article number of the product')
    name = models.CharField(max_length=50, help_text='Name of the product',
                            blank=True)

    def __str__(self):
        return self.id


class Tag(models.Model):
    id = models.CharField(max_length=32, primary_key=True, unique=True,
                          help_text='A unique tag ID')

    def __str__(self):
        return self.id


class Location(models.Model):
    id = models.CharField(max_length=32, primary_key=True, unique=True,
                          help_text='A unique name of the location')
    tag = models.OneToOneField(Tag)

    def __str__(self):
        return self.id


class Pallet(models.Model):
    tag1 = models.OneToOneField(Tag, related_name='tag1_on_pallet',
                                help_text='First tag')
    tag2 = models.OneToOneField(Tag, related_name='tag2_on_pallet',
                                help_text='Second tag')
    location = models.OneToOneField(Location, null=True, blank=True)

    def __str__(self):
        return ', '.join(
            ['%d %s' % (s.units, s.product.name) for s in self.products.all()])


class SetOfProduct(models.Model):
    product = models.ForeignKey(Product, related_name='in_sets',
                                help_text='Product')
    units = models.PositiveIntegerField(help_text='Units of the product')
    placed_on = models.ForeignKey(Pallet, related_name='products',
                                  help_text='Placed on pallet')

    def __str__(self):
        return '%d %s' % (self.units, self.product.name)


class Reader(models.Model):
    operator = models.CharField(max_length=20)

    def __str__(self):
        return self.operator


class Move(models.Model):
    pallet = models.ForeignKey(Pallet)
    moved_from = models.ForeignKey(Location, related_name='moved_away')
    moved_from_time = models.DateTimeField()
    moved_to = models.ForeignKey(Location, blank=True, null=True,
                                 related_name='moved_here')
    moved_to_time = models.DateTimeField(blank=True, null=True)
    reader = models.ForeignKey(Reader)

    def __str__(self):
        return 'Move "%s" from "%s" to "%s"' % \
               (self.pallet, self.moved_from, self.moved_to)
