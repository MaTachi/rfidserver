from django.contrib import admin
from warehouse.models import Tag, Product, Pallet, SetOfProduct, Location, \
    Reader, Move


admin.site.register(Product)
admin.site.register(Tag)
admin.site.register(Location)
admin.site.register(Pallet)
admin.site.register(SetOfProduct)
admin.site.register(Reader)


class MoveAdmin(admin.ModelAdmin):
    list_display = '__str__', 'reader'
    list_filter = 'reader',
admin.site.register(Move, MoveAdmin)
