from rest_framework import viewsets
from warehouse.models import Product, Tag, Location, Pallet, SetOfProduct, \
    Reader, Move


class ProductViewSet(viewsets.ModelViewSet):
    model = Product


class TagViewSet(viewsets.ModelViewSet):
    model = Tag


class LocationViewSet(viewsets.ModelViewSet):
    model = Location


class PalletViewSet(viewsets.ModelViewSet):
    model = Pallet


class SetOfProductViewSet(viewsets.ModelViewSet):
    model = SetOfProduct


class ReaderViewSet(viewsets.ModelViewSet):
    model = Reader


class MoveViewSet(viewsets.ModelViewSet):
    model = Move
