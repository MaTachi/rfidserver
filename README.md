# RFID Server

## Prerequisites

* Python 2.7 or 3.x (The instructions below assumes Python 3)
* virtualenv

### Linux

    $ sudo apt-get install python3 python-virtualenv

### Windows

Python: <http://www.python.org/download/releases/3.3.3/>  
Pip: <http://stackoverflow.com/questions/4750806/how-to-install-pip-on-windows>  
Virtualenv: `pip install virtualenv`


## Setup

    $ ./setup.sh

## Run local development server

    $ source env/bin/activate
    $ python3 manage.py runserver 192.168.x.y:8000

Now, visit the api at `http://192.168.x.y:8000/api` and the admin site at
`http://192.168.x.y:8000/admin`
