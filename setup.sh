#!/usr/bin/env sh

virtualenv -p /usr/bin/python3 env
env/bin/pip install Django PyYAML djangorestframework
env/bin/python3 manage.py syncdb
env/bin/python3 manage.py loaddata sample-data
